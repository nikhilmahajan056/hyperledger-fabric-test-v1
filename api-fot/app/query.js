const { Gateway, Wallets } = require('fabric-network');
const helper = require('./helper')

let query_connection;

let connect = async(username,org_name)=>{
        const ccp = await helper.getCCP(org_name);
        const walletPath = await helper.getWalletPath(org_name);
        const wallet = await Wallets.newFileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        let identity = await wallet.get(username);
        if (!identity) {
            console.log(`An identity for the user ${username} does not exist in the wallet, so registering user`);
            await helper.getRegisteredUser(username, org_name, true)
            identity = await wallet.get(username);
            console.log('Run the registerUser.js application before retrying');
            return;
        }

    const connectOptions = {
        wallet, identity: username, discovery: { enabled: true, asLocalhost: false }
    }

    query_connection = new Gateway();
    await query_connection.connect(ccp, connectOptions);
}

const query = async (channelName, chaincodeName, args, fcn, username, org_name) => {

    try {

        // Get the network (channel) our contract is deployed to.
        const network = await query_connection.getNetwork(channelName);

        // Get the contract from the network.
        const contract = await network.getContract(chaincodeName);
        let result = null;

        switch (fcn) {
            case "GetProductByUniqueID":
                console.log("======= Executing GetProductByUniqueID =======")
                result = await contract.evaluateTransaction(fcn, args[0]);
                // await gateway.disconnect();
                break;
            case "GetHistoryForProduct":
	        	console.log("======= Executing GetHistoryForProduct =======")
	        	result = await contract.evaluateTransaction(fcn, args[0]);
                // await gateway.disconnect();
	        	break;
            case "ProductExists":
                console.log("======= Executing ProductExists =======")
                result = await contract.evaluateTransaction(fcn, args[0]);
                // await gateway.disconnect();
                break;
            default:
                break;
        }
        console.log(`Transaction has been evaluated, result is: ${result.toString()}`);

        result = JSON.parse(result.toString());
    
        const response_payload = {
            result: result,
            error: false,
            errorData: null
        }

        console.log("Query Transaction Try Block Response Payload: ", response_payload)
        return response_payload

    } catch (error) {

        console.log(`Query Transaction Getting error: ${error}`)
        const response_payload = {
            result: null,
            error: true,
            errorData: error.message
        }
        console.log("Query Transaction Catch Block Response Payload: ", response_payload)
        return response_payload

    }
}

exports.query = query
exports.connect = connect